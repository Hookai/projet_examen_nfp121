package fr.n7.cnam.nfp121.pr01;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Iterator;

/**
 * Maj indique pour chaque lot les positions mises à jour (ou ajoutées)
 * lors du traitement de ce lot.
 *
 * @author	Xavier Crégut <Prenom.Nom@enseeiht.fr>
 */
public class Maj extends Traitement {
	
	private Donnees donnees;
	
	@Override
	public void traiter(Position position, double valeur) {
		if (position == null) {
			return;
		}
		
		if(!this.donnees.getListePosition().contains(position)) {
			this.donnees.traiter(position, valeur);
		}
		
		super.traiter(position, valeur);
	}
	
	@Override
	public void gererDebutLotLocal(String nomLot) {
		this.donnees = new Donnees();
		this.donnees.gererDebutLot(nomLot);
	}
	
	@Override
	public void gererFinLotLocal(String nomLot) {		
		System.out.println("Position mis à jour pour le lot " + nomLot + ": ");
		for(Iterator<Position> it = this.donnees.getListePosition().iterator(); it.hasNext();) {
			Position p = it.next();
			
			System.out.println("Position: " + p);
		}
	}
	
}


