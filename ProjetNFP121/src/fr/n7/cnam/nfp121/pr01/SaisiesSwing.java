package fr.n7.cnam.nfp121.pr01;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;


import javax.swing.*;

public class SaisiesSwing extends JFrame {

	private static final long serialVersionUID = 1L;
	
	final private JLabel abscisse = new JLabel("Abscisse : ");
	final private JLabel ordonnee = new JLabel("Ordonnee : ");
	final private JLabel valeur = new JLabel("Valeur : ");
	final private JTextField champAbcisse = new JTextField(15);
	final private JTextField champOrdonnee = new JTextField(15);
	final private JTextField champValeur = new JTextField(15);
	//boutons
	final private JButton valider = new JButton("Valider");
	final private JButton effacer = new JButton("Effacer");
	final private JButton terminer = new JButton("Terminer");
	final private File fichier = new File("test.txt");

	
	public SaisiesSwing() {
		super();
		
		// Création d'une fenêtre
		this.setTitle("Analyseur de données");          
	    
		// Insertion des panels dans la fenêtre
		JPanel mainPanel = new JPanel();
	    JPanel panel1 = new JPanel();
		JPanel panel2 = new JPanel();
		this.setContentPane(mainPanel);
		
		
		abscisse.setHorizontalAlignment(JLabel.RIGHT);
		ordonnee.setHorizontalAlignment(JLabel.RIGHT);
		valeur.setHorizontalAlignment(JLabel.RIGHT);
		
		//insertion des éléments dans panel1
		panel1.setLayout(new GridLayout(0,2));
		panel1.add(abscisse);
		panel1.add(champAbcisse);
		panel1.add(ordonnee);
		panel1.add(champOrdonnee);
		panel1.add(valeur);
		panel1.add(champValeur);
		
		//insertion des éléments dans panel2
		panel2.add(valider);
		panel2.add(effacer);
		panel2.add(terminer);
		
		mainPanel.setLayout(new GridLayout(0,1));
		mainPanel.add(panel1);
		mainPanel.add(panel2);
		
		this.pack();
		this.setVisible(true);
		
		//insertion des écouteurs aux boutons
		terminer.setEnabled(false);
		this.terminer.addActionListener(new ActionTerminer());
		this.effacer.addActionListener(new ActionEffacer());
		this.valider.addActionListener(new ActionValider());
		
		
    	
	}
	
	public class ActionTerminer implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			System.out.println("Arrêt du programme");
			System.exit(1);
		}
		
	}
	
	public class ActionEffacer implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			if (e.getSource() == effacer){
				System.out.println("Suppression des valeurs");
				champAbcisse.setText("");
				champOrdonnee.setText("");
				champValeur.setText("");
			}
		}
	}
	
	public class ActionValider implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent e) {
			boolean champValide = true;
			if (! entierValide(champAbcisse.getText())) {
				champAbcisse.setBackground(Color.RED);
				champValide = false;
			}
			else {
				champAbcisse.setBackground(Color.WHITE);
			}
			
			if (! entierValide(champOrdonnee.getText())) {
				champOrdonnee.setBackground(Color.RED);
				champValide = false;
			}
			else {
				champOrdonnee.setBackground(Color.WHITE);
			}
			
			if (! doubleValide(champValeur.getText())) {
				champValeur.setBackground(Color.RED);
				champValide = false;
			}
			else {
				champValeur.setBackground(Color.WHITE);
			}
			
			if (champValide) {
				System.out.println(champAbcisse.getText());
				System.out.println(champOrdonnee.getText());
				System.out.println(champValeur.getText());
				terminer.setEnabled(true);
				System.out.println("Écriture des valeurs dans le fichier test.txt");
				save(fichier, champAbcisse.getText(),champOrdonnee.getText(),champValeur.getText());
			}
			else {
				System.out.println("Veuillez remplir tous les champs correctement");
				terminer.setEnabled(false);
			}
		}
	}


	public void save(File f, String abs, String ord, String val){
		FileWriter out;
		try {
			out = new FileWriter(f, true);
			PrintWriter pw = new PrintWriter(out);
			pw.print(abs + " ");
			pw.print(ord + " ");
			pw.println(val + " ");
			pw.close();
		} catch (IOException e) {
			System.out.println("Problème rencontré lors de l'écriture dans le fichier test.txt");
		}
    	
	}
	
	
	private boolean entierValide(String valeur)
	{
		try {
			Integer.parseInt(valeur);
		} catch (NumberFormatException f) {
			return false;
		}
		
		return true;
	}
	
	public boolean doubleValide(String valeur) {
		try {
			Double.parseDouble(valeur);
		} catch (NumberFormatException f) {
			return false;
		}
		
		return true;

	}
	


	public static void main(String[] args) {
		new SaisiesSwing();
	}
}
