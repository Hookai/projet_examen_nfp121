package fr.n7.cnam.nfp121.pr01;


/** Définir une position.  */
public class Position {
	public int x;
	public int y;

	public Position(int x, int y) {
		this.x = x;
		this.y = y;
		
	}
	@Override
	public int hashCode() {
		return java.util.Objects.hash(this.x, this.y);
	}

	@Override
	//Compare les positions
	public boolean equals(Object object) {
		if(object == null) {
			return false;
		}
		


		Position posObj = (Position)object;
		return (this.x == posObj.x && this.y == posObj.y);
	}


	@Override public String toString() {
		return super.toString() + "(" + this.x + "," + this.y + ")";
	}
}
