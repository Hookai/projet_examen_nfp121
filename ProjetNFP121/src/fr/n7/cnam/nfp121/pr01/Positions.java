package fr.n7.cnam.nfp121.pr01;
import java.util.ArrayList;
/**
  * Positions enregistre toutes les positions, quelque soit le lot.
  *
  * @author	Xavier Crégut <Prenom.Nom@enseeiht.fr>
  */
public class Positions extends PositionsAbstrait {

	private ArrayList<Position> listePosition = new ArrayList<Position>();
	
	@Override
	public void traiter(Position position, double valeur) {
		this.listePosition.add(position);		
		super.traiter(position, valeur);
	}

	@Override
	public int nombre() {
		return this.listePosition.size();
	}
	
	@Override
	public Position position(int indice) {
		return this.listePosition.get(indice);
	}

	@Override
	public int frequence(Position position) {
		int freq = 0;

		for (Position pos : this.listePosition) {
			if (pos.equals(position)) {
				freq += 1;
			}
		}

		return freq;
	}
}




