package fr.n7.cnam.nfp121.pr01;

import java.util.*;

/**
  * SommeParPosition 
  *
  * @author	Xavier Crégut <Prenom.Nom@enseeiht.fr>
  */

public class SommeParPosition extends Traitement {

	private Donnees donnees;
	
	@Override
	public void gererDebutLotLocal(String nomLot) {
		this.donnees = new Donnees();
		this.donnees.gererDebutLotLocal(nomLot);
	}
	
	@Override
	public void traiter(Position position, double valeur) {
		if (position == null) {
			return;
		}
		
		if(this.donnees.getListePosition().contains(position)) {
			int index = this.donnees.getListePosition().indexOf(position);
			this.donnees.getListeDouble().set(index, this.donnees.getListeDouble().get(index) + valeur);
		} else {
			this.donnees.traiter(position, valeur);
		}
				
		super.traiter(position, valeur);
	}
	
	@Override
	public void gererFinLotLocal(String nomLot) {
		System.out.println("La somme par position pour le lot " + nomLot + " est : ");
		
		for(int i = 0; i < this.donnees.getListePosition().size(); i++) {
			System.out.println("La Position: " + this.donnees.getListePosition().get(i) + " a pour valeur: " + this.donnees.getListeDouble().get(i));
		}
	}
}
