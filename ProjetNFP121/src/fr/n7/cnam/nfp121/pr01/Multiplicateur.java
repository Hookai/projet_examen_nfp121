package fr.n7.cnam.nfp121.pr01;

/**
  * Multiplicateur transmet la valeur multipliée par un facteur.
  *
  * @author	Xavier Crégut <Prenom.Nom@enseeiht.fr>
  */
public class Multiplicateur extends Traitement {

	private double multiplicateur;
	
	public Multiplicateur(double multiplicateur) {
		this.multiplicateur = multiplicateur;
	}
	
	@Override
	public void traiter(Position position, double valeur) {
		valeur = valeur * this.multiplicateur;
		super.traiter(position, valeur);
	}
}

