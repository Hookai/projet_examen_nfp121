package fr.n7.cnam.nfp121.pr01;


import java.io.FileOutputStream;
import java.io.IOException;

import org.jdom2.Attribute;
import org.jdom2.DocType;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

/**
 * GenerateurXML écrit dans un fichier, à charque fin de lot, toutes
 * les données lues en indiquant le lot dans le fichier XML.
 *
 * @author	Xavier Crégut <Prenom.Nom@enseeiht.fr>
 */
public class GenerateurXML extends Traitement {

	private String fileName;
	private Element rootElem;
	private Element lotElem;
	private Document doc;
	private Donnees data;

	public GenerateurXML(String fileName) {
		this.fileName = fileName;
		this.rootElem = new Element("Resultat"); 
		this.doc = new Document(this.rootElem, new DocType("generateur.dtd"));
	}
	
	@Override
	protected void gererDebutLotLocal(String nomLot) {
		this.data = new Donnees();
		this.data.gererDebutLot("Data " + nomLot);
		this.lotElem = new Element("Lot");
		this.lotElem.setAttribute("nom", nomLot);
		this.rootElem.addContent(this.lotElem);
	}
	
	@Override
	public void traiter(Position position, double valeur) {
		this.data.traiter(position, valeur);	
		super.traiter(position, valeur);
	}

	@Override
	protected void gererFinLotLocal(String nomLot) {
		this.data.gererFinLot("Data " + nomLot);
		
		for (int i = 0; i < this.data.getListePosition().size(); i++) {
			Position position = this.data.getListePosition().get(i);
			double value = this.data.getListeDouble().get(i);
			
			Element elemPosition = new Element("Pos");
			
			elemPosition.setAttribute(new Attribute("x", String.valueOf(position.x)));
			elemPosition.setAttribute(new Attribute("y", String.valueOf(position.y)));

			Element valeur = new Element("valeur");
			valeur.setText(String.valueOf(value));
			elemPosition.addContent(valeur);
			this.lotElem.addContent(elemPosition);
		}
		
	    XMLOutputter xmlOutput = new XMLOutputter(Format.getPrettyFormat());
		try {
			FileOutputStream docOutput = new FileOutputStream(this.fileName);
			xmlOutput.output(this.doc, docOutput);
			docOutput.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
	
}

