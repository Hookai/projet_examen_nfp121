package fr.n7.cnam.nfp121.pr01;
import java.util.ArrayList;
import java.util.List;
/**
  * Donnees enregistre toutes les données reçues, quelque soit le lot.
  *
  * @author	Xavier Crégut <Prenom.Nom@enseeiht.fr>
  */

@SuppressWarnings("unused")
public class Donnees extends Traitement {

	private ArrayList<Position> ListePosition;
	private ArrayList<Double> ListeDouble;

	public Donnees() {
		this.ListePosition = new ArrayList<Position>();
		this.ListeDouble = new ArrayList<Double>(); 
	}
	
	@Override
	public void traiter(Position position, double valeur) {
		this.ListePosition.add(position);
		this.ListeDouble.add(valeur);
		super.traiter(position, valeur);
	}
	
	public ArrayList<Position> getListePosition() {
		return this.ListePosition;
	}
	
	public ArrayList<Double> getListeDouble() {
		return this.ListeDouble;
	}
	
}

