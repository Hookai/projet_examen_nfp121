package fr.n7.cnam.nfp121.pr01;

public class CycleException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public CycleException() {
		super("Ce traitement est déjà présent dans la liste des traitements à venir");
	}
	
}

