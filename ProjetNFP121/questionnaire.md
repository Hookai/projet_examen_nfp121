% NOM Prénom (numéro) : ... (...)


**Remarque :** Ce fichier est au format Markdown.  À partir de lui, on peut
produire un pdf, un odt, un docx, du html, etc.  Par exemple, avec pandoc on
pourra faire :

~~~
pandoc --toc -o questionnaire.pdf questionnaire.txt
pandoc --toc -s -o questionnaire.html questionnaire.txt
~~~


# Points non traités du sujet

traitement supplémentaire


# Traitements


## Quelle est la classe du traitement choisi ?

...


## Pourquoi avoir fait de traitement une classe abstraite et non une interface ?

Cela permet de mettre à disposition des méthodes possédant un comportement prédéfini aux classes qui héritent de la classe abstraite traitement, elles pourront donc être redéfinies.


## Pourquoi certaines méthodes sont déclarées `final` ?

Afin que les classes héritant de ces méthodes ne puissent pas les surcharger ou les redéfinir afin de conserver le comportement initial de ces dernières souhaité par le programmeur.


## La classe Traitement est abstraite alors qu'elle ne contient pas de méthodes abstraites. Pourquoi ?

Car cette classe permet la mise en place d'un chaînage de traitements, elle permettra donc au développeur d'unifier les comportements des traitements implémentés.


## Est-ce que de faire de Traitement une classe abstraite est vraiment logique ici ?

Oui car cela permettra au développeur d'implémenter les méthodes afin d'unifier les comportements de celles-ci ce qui est souhaité par le développeur.


## Pour le traitement Normaliseur, quels autres traitements avez-vous utilisés et comment ?

le traitement Données, qui permettra de récupérer les données traitées ainsi que le traitement Max qui calcule le max des valeurs vues quelque soit le lot.


## Quelles modifications avez-vous été obligés de faire sur la classe Position ?

J'ai dû définir la méthode equals() pour pouvoir comparer 2 positions dans le but de comparer X et Y dans frequence(). Puis définir la méthode hashcode() pour plus de précision dans la comparaison.



# Remarques sur Swing

L'interface swing réalisée est fonctionnel, cependant l'esthéthique ne correspond pas totalement à ce qui est attendu et pourrait être modifié. 
0

# Remarques sur l'Introspection

Le concept de l'introspection à globalement été compris pour ma part, cependant j'ai eu assez de mal à le programmer.


# Remarques sur XML

## Lecture d'un document XML

...


## Production d'un document XML

Les données du documents sont récupérées via des listes dans le traitement Données et ce dernier est crée grâce à la librairie JDOM.


# Principaux choix faits

...


# Critiques de l'architecture proposée et améliorations possibles

Le sujet, bien qu'étant intéressant à été assez compliqué à mettre en oeuvre pour ma part. Un sujet plus concret avec une mise en situation plus "réelle" aurait peut-être été préférable.


# Difficultés rencontrées

Compréhension et application de certaines notions liées au sujet (Introspection), et réalisation d'un traitement supplémentaire. 

